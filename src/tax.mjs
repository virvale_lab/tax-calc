export default class Tax {
    /**
     * Apply tax to the list price.
     * @param {number} list_price before tax
     * @param {number} tax percentage
     * @returns {number} after tax (total_price)
     */
    static applyTax(list_price, tax) {
        return (list_price + list_price * tax / 100);
    }
    static deductTax(total_price, tax) {
        return (total_price / (1 + (tax / 100)));
    }
    static figureTaxRate(list_price, total_price) {
        return ((1 / (list_price / total_price)) - 1 ) * 100;
    }
}